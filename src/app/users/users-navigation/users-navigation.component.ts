import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

@Component({
  selector: 'app-users-navigation',
  templateUrl: './users-navigation.component.html',
  styleUrls: ['./users-navigation.component.css']
})
export class UsersNavigationComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  	var value = localStorage.getItem('token');
    
    if(!value || value == undefined || value == "" || value.length == 0){    
      this.router.navigate(['/login']);
    }
  }

}
